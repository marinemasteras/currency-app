package com.szypulski.currencyapp.model.enums;

public enum AlertType {

  OVER,
  UNDER
}
