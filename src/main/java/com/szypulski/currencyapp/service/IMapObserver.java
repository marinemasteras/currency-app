package com.szypulski.currencyapp.service;

import java.util.Map;

public interface IMapObserver {

  void update(Map<String, Double> stringDoubleMap);

}
